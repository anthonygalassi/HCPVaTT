#!/usr/bin/env python

from __future__ import print_function

import util
from os.path import expanduser


def interface(cfg, whitelist):
    for path in util.ids_to_scan_paths(cfg, *whitelist):
        yield path


def cli_interface(arguments):
    cfg = expanduser(arguments.cfg)
    whitelist = arguments.whitelist
    with open(whitelist) as fd:
        for path in interface(cfg, set(map(str.strip, fd.readlines()))):
            print(path)


def generate_parser(parser):
    parser.set_defaults(func=cli_interface)
    return parser
