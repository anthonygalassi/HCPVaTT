#!/usr/bin/env python

from __future__ import print_function

from collections import Counter, defaultdict, namedtuple
from operator import attrgetter
import functools
import sys
import util
from util import compose, safeattrgetter, safeitemgetter


def first_such(pipes, cond):
    for pname in pipes:
        if cond(pipes[pname]):
            return pname
    return None


def last_such(pipes, cond):
    for pname in reversed(pipes):
        if cond(pipes[pname]):
            return pname
    return None


csv_print = functools.partial(print, sep=',', file=sys.stdout)


def interface(cfg, whitelist, blacklist, not_bad, not_good, only_complete, number_status):
    pnames = list(util.get_pipes(cfg))
    columns = ['suid', 'visit', 'stid', 'target']
    columns.extend(pnames)
    Unit = namedtuple('Unit', columns)

    summary = defaultdict(int)

    for path in util.get_scan_paths(cfg, blacklist=blacklist, whitelist=whitelist):
        suid = util.path_to_id(cfg, path)
        stid = util.path_to_study(cfg, path)
        visit = util.path_to_visit(cfg, path)

        try:
            pipes = util.scan_path_to_pipe_meta(cfg, path)
        except util.MalformedJsonExcption as e:
            print('MalformedJsonExcption', suid, visit, stid, e, sep=',', file=sys.stderr)
            continue

        target = first_such(pipes, lambda p: p.status not in {1, 999})

        status = [pipes[p].status for p in pipes]

        c = Counter(map(attrgetter('status'), pipes.values()))

        if not_bad:
            fail = lambda c: 2 in c or 3 in c or 4 in c
            if fail(c): # and not cascade(c):
                continue

        if not_good:
            succ = lambda c: not bool(set(c.keys()).difference({1, 999}))
            if succ(c):
                continue

        if only_complete:# XXXPMR: 2 is non-informative
            if None in c:
                continue

        proxy = target if target is not None else 'SUCCESS'
        summary[proxy] += 1
        if not number_status:
            status = (util.status_codes[pipes[p].status] for p in pipes)

        yield Unit(suid, visit, stid, target, *status)

    print('SUMMARY\n-----------', file=sys.stderr)
    for target in summary:
        print('{:<25}'.format(target), ':', summary[target], file=sys.stderr)


def cli_interface(arguments):
    cfg = arguments.cfg
    whitelist = arguments.whitelist
    blacklist = arguments.blacklist
    no_bad = arguments.no_bad
    no_good = arguments.no_good
    only_complete = arguments.only_complete
    number_status = arguments.number_status

    getlist = lambda fd: set(map(str.strip, fd.readlines()))
    with open(blacklist) as bd:
        bl = getlist(bd)

    if whitelist is not None:
        with open(whitelist) as wd:
            wl = getlist(wd)
    else:
        wl = set()

    for i, msg in enumerate(interface(cfg, wl, bl,
                                      no_bad, no_good, only_complete, number_status)):
        if i == 0:
            csv_print(*msg._fields)
        csv_print(*msg)


def generate_parser(parser):
    parser.add_argument('--no-good', action='store_true',
                        help='only yield bad runs',
                        default=False)
    parser.add_argument('--no-bad', action='store_true',
                        help='only yield bad runs',
                        default=False)
    parser.add_argument('--only-complete', action='store_true',
                        help='only yield bad runs',
                        default=False)
    parser.add_argument('--number-status', action='store_true',
                        help='yield integer status, instead of string',
                        default=False)

    parser.set_defaults(func=cli_interface)
    return parser
