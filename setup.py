#!/usr/bin/env python

import os
from setuptools import setup


def read(fname):
  return open(os.path.join(os.path.dirname(__file__), fname)).read()

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from os import path

here = path.abspath(path.dirname(__file__))

setup(
  name         = 'PipeVaTT',
  version      = '0.2.1',
  description  = 'Pipeline Validator and Triage Tool',
  author       = 'Philip Robinson',
  author_email = 'robinsph@ohsu.edu',
  license      = 'MIT',

  classifiers  = [
    # How mature is this project? Common values are
    #   3 - Alpha
    #   4 - Beta
    #   5 - Production/Stable
    'Development Status :: 3 - Alpha',

    # Indicate who your project is intended for
    'Intended Audience :: Developers',
    'Topic :: Software Development :: Build Tools',

    # Pick your license as you wish (should match "license" above)
    'License :: OSI Approved :: MIT License',

    # Specify the Python versions you support here. In particular, ensure
    # that you indicate whether you support Python 2, Python 3 or both.
    'Programming Language :: Python :: 2.7',
    'Programming Language :: Python :: 3',
    ],

  url          = 'https://gitlab.com/Fair_lab/HCPVaTT',
  packages     = ['pipevatt'],
  entry_points = { # enable cmd-line access
    "console_scripts": [
      'pipevatt = pipevatt.main:main',
    ]
    },
  include_package_data=True,
  package_data={'pipevatt':['configs/*']},
  scripts = [],
  long_description=read('README.md'),
  install_requires = [
    'argparse==1.4.0',
  #  'wsgiref==0.1.2'
  ]
)
