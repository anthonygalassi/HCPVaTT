#!/usr/bin/env bash

set -ueo pipefail

if (( $# != 1)); then
    printf "Usage: %s <report-directory>\n" "$0" >&2 ;
    exit 1;
fi;

reportdir=$1

pushd $reportdir

for k in $(du -sh sub-* | awk -F' ' '{print $1}' | sort | uniq);
do
    mkdir $k;
    for s in $(du -sh sub-* | grep $k | awk -F' ' '{print $2}');
    do
        mv $s $k;
    done;
done;

popd
