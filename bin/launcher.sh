#!/usr/bin/env bash

set -x
set -euo pipefail
ENG=/home/users/robinsph/git/live.hcponeclick
VAT=/home/users/robinsph/git/HCPVaTT

for ID in `tail -n +2 ${VAT}/targets.txt | awk -F, '{print $1}'`; do
    SDIR=`${VAT}/./hcpvatt sidpath --whitelist=<(echo ${ID}) --cfg ${VAT}./exahead1_abcd.json`"/ses-baselineYear1Arm1/HCP_release_20170910_v1.1/"
    ${ENG}/./run_oneclick.py  -s ${SDIR} -id ${ID} -U
done
