#!/usr/bin/env bash

set -euo pipefail

if (( $# != 1 )); then
    printf "Usage: %s <audit_file.txt> \n" "$0" >&2 ;
    exit 1;
fi;

FILENAME=$1

for NODE in HcpGenericWrapper HcpPre HcpFree HcpPost \
            HcpVol HcpSurf FNLpreproc_v2 task_fMRI \
            HcpTask ExecSummary ICA_AROMA HcpSurfICA \
            FNLpreproc_v2_ICA task_fMRI_ICA HcpTask_ICA \
            ExecSummary_ICA;
do
    grep ${NODE} ${FILENAME} > ${NODE}_targets.txt
done;
